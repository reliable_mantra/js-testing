const expect = require("../../lib").expect;
const test = require("../../lib").test;
const thumbWar = require("../thumb-war");
const utils = require("../utils");

function fn(impl = () => {}) {
  const mockFn = (...args) => {
    mockFn.mock.calls.push(args)
    return impl(...args)
  };
  mockFn.mock = {calls: []};
  return mockFn;
}

test("returns winner -> mock-fn", () => {
  const originalGetWinner = utils.getWinner;
  utils.getWinner = fn((p1, p2) => p1);

  const winner = thumbWar("Kent C. Dodds", "Ken Wheeler");
  expect(winner).toBe("Kent C. Dodds");
  expect(utils.getWinner.mock.calls).toEqual([
    ["Kent C. Dodds", "Ken Wheeler"],
    ["Kent C. Dodds", "Ken Wheeler"]
  ]);

  // cleanup
  utils.getWinner = originalGetWinner;
});
