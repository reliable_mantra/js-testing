require("../__no-framework-mocks__/utils"); // prime the cache
const utilsPath = require.resolve("../utils");
const mockUtilsPath = require.resolve("../__no-framework-mocks__/utils");
require.cache[utilsPath] = require.cache[mockUtilsPath];

const expect = require("../../lib").expect;
const test = require("../../lib").test;
const thumbWar = require("../thumb-war");
const utils = require("../utils");

test("returns winner -> external-mock-module", () => {
  const winner = thumbWar("Kent C. Dodds", "Ken Wheeler");
  expect(winner).toBe("Kent C. Dodds");
  expect(utils.getWinner.mock.calls).toEqual([
    ["Kent C. Dodds", "Ken Wheeler"],
    ["Kent C. Dodds", "Ken Wheeler"]
  ]);

  // cleanup
  delete require.cache[utilsPath];
});
