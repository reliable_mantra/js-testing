/**
 * Simplest JavaScript Tests
 * 
 * Test is simply code that will throw an error 
 * when the result is not what we expect.
 * 
 * The job of testing frameworks is to make the thrown error 
 * message as useful as possible so we can quickly identify 
 * what the problem is and fix it.
 */
const sum = (a, b) => a + b;
const subtract = (a, b) => a - b;

let result, expected;

result = sum(3, 7);
expected = 10;
if (result !== expected) {
  throw new Error(`${result} is not equal to ${expected}`);
}

result = subtract(7, 3);
expected = 4;
if (result !== expected) {
  throw new Error(`${result} is not equal to ${expected}`);
}



/**
 * Abstract Test Assertions 
 * into a JavaScript Assertion Library
 * 
 * Fundamentaly, this "library", takes an actual value 
 * and then it returns an object that has functions 
 * for different assertions that we can make on
 * that actual value.
 */
function expect(actual) {
  return {
    toBe(expected) {
      if (actual !== expected) {
        throw new Error(`${actual} is not equal to ${expected}`);
      }
    },
    toEqual(expected) {},
    toBeGreaterThen(expected) {}
  }
};

result = sum(3, 7);
expected = 10;
// expect(result).toBe(expected);

result = subtract(7, 3);
expected = 4;
// expect(result).toBe(expected);


/**
 * Improve testing library
 * 
 * - previous library would stop at the first failed test
 * - the line where the error occured would allways be at the same place where the error was thrown
 * - helpful error messages
 * - enable async tests
 */
async function test(title, callback) {
  try {
    await callback();
    console.log(`✓ ${title}`);
  } catch (error) {
    console.error(`✕ ${title}`);
    console.error(error);
  }
};

const sumAsync = (a, b) => Promise.resolve(a + b);
const subtractAsync = (a, b) => Promise.resolve(a - b);

test("sumAsync adds numbers asynchronously", async () => {
  const result = await sumAsync(3, 7);
  const expected = 10;
  expect(result).toBe(expected);
});

test("subtractAsync subtracts numbers asynchronously", async () => {
  const result = await subtractAsync(7, 3);
  const expected = 5;
  expect(result).toBe(expected);
});
