const _ = require("lodash");

function expect(actual) {
  return {
    toBe(expected) {
      if (actual !== expected) {
        throw new Error(`Expected ${actual} to be ${expected}`);
      }
    },
    toEqual(expected) {
      if (!_.isEqual(actual, expected)) {
        throw new Error(`Expected ${actual} to be equal to ${expected}`);
      }
    },
    toBeGreaterThen(expected) {}
  }
};

async function test(title, callback) {
  try {
    await callback();
    console.log(`✓ ${title}`);
  } catch (error) {
    console.error(`✕ ${title}`);
    console.error(error);
  }
};

module.exports = {
  expect,
  test
};
