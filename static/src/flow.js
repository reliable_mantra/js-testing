// @flow

/**
 * Flow analysis example
 */
function multiply(a, b) {
  return a * b;
}

multiply(1, 2);

/**
 * Uninitialized references
 */
function getLastInitial(person) {
  const { lastName } = person; // fails if person not defined
  return lastName ? lastName[0] : "";
}

const person = {
  firstName: "Richard",
  middleName: "Mark",
  lastName: "Volkman"
};
console.log(getLastInitial(person)); // good: outputs "V"

// error
// let p;
// console.log(getLastInitial(p));

/**
 * Inferred type
 * Type of scope will not get inferred right away
 * because it can change so scopes flow will be
 * followed so that changes can be caought 

 * Bottom line, it's better to type it right away
 */
let score = 0;
console.log(score);

/**
 * Maybe type
 */
let nullableScore: ?number = 0;
console.log(nullableScore);

/**
 * Basic types
 *
 * primitives: boolean, number, string (number includes Infinity and NaN)
 * array: boolean[], number[], string[]
 * null: only matches JavaScript null value
 * void: type of undefined and functions that don't return anything
 * any: means any type is allowed
 * mixed: similar to any, but must perform runtime type checks before using value
 */
function getChars(text: string, count: number, fromStart: boolean): string {
  return fromStart ? text.substring(0, count) : text.substr(-count);
}
getChars("abcdefg", 3, true);

function printNumberArray(numbers: number[]) {
  return numbers.join(", ");
}
printNumberArray([1, 2, 3]);

function mixedFoo(v: mixed) {
  if (typeof v === "number") return v * 2;
  if (typeof v === "string") return v.length;
  return v;
}
mixedFoo(2);

/**
 * Builtin Types
 *
 * Built-in JavaScript constants, functions, objects and classes
 * Types from browser Document Object Model(DOM)
 * Types from browser API (GeoLocation, History ...)
 * Types from Node.js standard library
 * Types from React (Synthetic*Event ...)
 */

/**
 * Type Aliases
 *
 * type SomeNameType = some-type;
 */
type StudentType = {
  name: string,
  age: number
};

function sayHello(student: StudentType): string {
  return `Hi ${student.name}`;
}

sayHello({ name: "Mary", age: 35 });

/**
 * Function Types
 */
function doSomething(callback: (x: number, y: number, z?: number) => number) {
  return callback(5, 3);
}

doSomething((x, y, z = null) => (!z ? x * y : x * y * z));

/**
 * Object Types
 */
type PersonType = {
  name: string,
  birthday: Date,
  spouse?: ?PersonType
};

const tami: PersonType = {
  name: "Tami",
  birthday: new Date(1961, 8, 9),
  height: 65
};

const mark: PersonType = {
  name: "Mark",
  birthday: new Date(1961, 3, 16),
  height: 74,
  spouse: tami
};
console.log(mark);

/**
 * Classes
 */
class Person {
  name: string;
  birthday: Date;
  height: number;
  spouse: Person;

  constructor(name: string, birthday: Date, height: number): void {
    this.name = name;
    this.birthday = birthday;
    this.height = height;
  }

  marry(person: Person): void {
    this.spouse = person;
    person.spouse = this;
  }
}

const susy: Person = new Person("Susy", new Date(1961, 8, 9), 65);
const john: Person = new Person("John", new Date(1961, 3, 16), 74);
susy.marry(john);

function logPerson(person: Person): void {
  const status: string = person.spouse
    ? "married to " + person.spouse.name
    : "single";
  console.log(person.name + " is " + status + ".");
}
logPerson(john);

/**
 * Unions
 */
type PrimitiveType = boolean | number | string;
let value: PrimitiveType = true;
value = 7; // good
value = "foo"; // good
// error
//  value = {};
console.log(value);

type AnimalType = { name: string, type: "animal", legCount: number };
type MineralType = { name: string, type: "mineral", hardness: number };
type VegetableType = { name: string, type: "vegetable", color: string };
type ThingType = AnimalType | MineralType | VegetableType;

const dog: AnimalType = { name: "Dasher", type: "animal", legCount: 4 };
const mineral: MineralType = { name: "amethyst", type: "mineral", hardness: 7 };
const vegetable: VegetableType = {
  name: "corn",
  type: "vegetable",
  color: "yellow"
};

let thing: ThingType = dog;
thing = mineral;
thing = vegetable;
// error
// thing = {name: "bad", type: "other"};

/**
 * Type Discrimination
 */
function processThing(thing: ThingType) {
  // error
  // console.log(`${thing.name} is a ${thing.color} vegetable.`);

  // forces you to check all types
  switch (thing.type) {
    case "animal":
      console.log(`${thing.name} is an animal with ${thing.legCount} legs.`);
      break;
    case "mineral":
      console.log(
        `${thing.name} is a mineral with hardness of ${thing.hardness}.`
      );
      break;
    case "vegetable":
      console.log(`${thing.name} is a ${thing.color} vegetable.`);
      break;
  }
}

processThing(thing);
